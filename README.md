# Locastic takehome front-end assinment

This project is deployed at Netlify.
Visit [locastic-takehome](https://splendorous-meringue-59bde9.netlify.app/)

## What's taken care of

1. Project structure
2. general coding patterns & practices I follow
3. Data fetching combined with state management
4. Reusability and scalability

## What's neglected

1. `Cart & checkout form` (I believe the above code is sufficient to portray my coding abilities in general)
2. `Design & responsiveness` (Can be implemented. I was short on time so didn't bother to spend time on it)
3. `Unit & Integration Tests` (This was definatley something on my cards but missed on it due to lack of time)

## Important note

If there's anything else that you feel I should implemet to express my skill better you can always reach me out. Because I believe communication is key before making any judgments or final call.
Also if required I can finish the complete assignment on the weekend. Just ping me up.

## How to run

1. clone the repositry
2. npm i
3. npm start
4. npx json-server --watch db.json
